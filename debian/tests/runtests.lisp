(require "asdf")

(let ((asdf:*user-cache* (uiop:getenv "AUTOPKGTEST_TMP"))) ; Store FASL in some temporary dir
  (asdf:load-system "nibbles/tests"))

;; Can't use ASDF:TEST-SYSTEM, its return value is meaningless
(let ((nibbles-tests::*output-directory* (uiop:getenv "AUTOPKGTEST_TMP")))
  (unless (rtest:do-tests)
    (uiop:quit 1)))
